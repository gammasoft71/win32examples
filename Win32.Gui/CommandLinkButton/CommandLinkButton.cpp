#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

#include <string>
#include <Windows.h>
#include <CommCtrl.h>

using namespace std;
using namespace std::literals;

HWND form = nullptr;
HWND commandLinkButton1 = nullptr;
HWND label1 = nullptr;
WNDPROC defWndProc = nullptr;
int commandLinkButton1Clicked = 0;

LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
  if (message == WM_CLOSE && hwnd == form) PostQuitMessage(0);
  if (message == WM_COMMAND && HIWORD(wParam) == BN_CLICKED && (HWND)lParam == commandLinkButton1) {
    wstring result = L"commandLinkButton1 clicked "s + to_wstring(++commandLinkButton1Clicked) + L" times"s;
    SendMessage(label1, WM_SETTEXT, 0, (LPARAM)result.c_str());
  }
  return CallWindowProc(defWndProc, hwnd, message, wParam, lParam);
}

int main(int argc, char* argv[]) {
  form = CreateWindowEx(0, WC_DIALOG, L"CommandLinkButton example", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, 300, 300, nullptr, nullptr, nullptr, nullptr);
  commandLinkButton1 = CreateWindowEx(0, WC_BUTTON, L"Link", BS_COMMANDLINK | WS_CHILD | WS_VISIBLE, 30, 30, 200, 60, form, nullptr, nullptr, nullptr);
  SendMessageW(commandLinkButton1, BCM_SETNOTE, 0, (LPARAM)L"Information text");

  label1 = CreateWindowEx(0, WC_STATIC, L"commandLinkButton1 clicked 0 times", WS_CHILD | WS_VISIBLE, 30, 100, 200, 46, form, nullptr, nullptr, nullptr);
  
  defWndProc = (WNDPROC)SetWindowLongPtr(form, GWLP_WNDPROC, (LONG_PTR)WndProc);
  ShowWindow(form, SW_SHOW);

  MSG message = {0};
  while (GetMessage(&message, nullptr, 0, 0))
    DispatchMessage(&message);
  return (int)message.wParam;
}
